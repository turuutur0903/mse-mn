'use client'
import Image from 'next/image';
import ServiceCard from '@/app/component/common/card/service';

import Apple from '@/app/component/common/card/service/Apple';
import NewYork from '@/app/component/common/card/service/newyork';
import Fed from '@/app/component/common/card/service/fed';
import { useState } from 'react';
import { data } from './data';
import { title } from 'process';





export default function Second() {

  const [isActiveMostRead, setIsActiveMostRead] = useState(false);
  const [isActiveLatest, setIsActiveLatest] = useState(false);
  const [isActiveLine, setIsActiveLine] = useState(false);

  const toggleMostRead = () => {
    setIsActiveMostRead(!isActiveMostRead);
    setIsActiveLatest(false); // Reset the other state
    setIsActiveLine(!isActiveLine);
  };

  const toggleLatest = () => {
    setIsActiveLatest(!isActiveLatest);
    setIsActiveMostRead(false); // Reset the other state
    setIsActiveLine(!isActiveLine);
  };
 
  return (
<div style={{ fontFamily: 'Rubik, sans-serif' }}>
    <div>
      <section className="relative h-screen bg-cover bg-center" style={{ backgroundImage: 'url(/forest.jpg)' }}>
        <div className="absolute inset-0 bg-black opacity-50"></div>
        <header className="absolute top-0 left-0 w-full bg-transparent text-white py-2 z-20">
  <div className="container mx-auto flex justify-between items-center " style={{ fontFamily: 'Rubik, sans-serif' }}>
    <nav className="flex space-x-4 ml-auto">
      <a href="#" className="hover:text-gray-300 font-bold my-3">БИДНИЙ ТУХАЙ</a>
      <a href="#" className="hover:text-gray-300 font-bold my-3">ҮЙЛЧИЛГЭЭ</a>
      <a href="#" className="hover:text-gray-300 font-bold my-3">ХОЛБОО БАРИХ</a>
      <a href="#" className="hover:text-gray-300 font-bold my-3">ЗАХ ЗЭЭЛ</a>
    </nav>
  </div>
</header>

        <div className="absolute top-20 left-0 w-full bg-transparent text-white py-2 z-20">
          <div className="container mx-auto flex justify-between items-center">
            <Image src="/logo.png" alt="TDB Securities Logo" width={150} height={75} />
            <nav className="flex space-x-4">
              <a href="#" className="hover:text-gray-300 font-bold text-xl" >Хөрөнгө оруулалт</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Судалгаа</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Мэдээ</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Зах зээл</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Боловсрол</a>
            </nav>
            <div className="space-x-4">
              <a href="# "className="hover:text-gray-300">Нэвтрэх</a>
              <button className="bg-green-500 hover:bg-green-600 text-black px-4 py-2 rounded-full font-bold">Данс Нээх</button>
            </div>
          </div>
        </div>
        <div className="relative z-10 flex justify-center items-center h-full font-rubik">
          <div className="text-center text-white ">
            <h2 className="text-6xl font-bold font-rubik">Your Partner In Mongolian Capital Market</h2>
            <p className="mt-4 py-9 text-3xl">Your Way To Wall Street</p>
            <button className="mt-6 bg-white bg-opacity-75 hover:bg-opacity-50 text-green-700 hover:text-green-600 px-7 py-4 rounded-full font-bold text-xl">Find out more</button>
          </div>
        </div>
      </section>
      <main>
      <section className="py-12 bg-white">
  <div className="container mx-auto text-center">

    <h2 className="text-6xl font-bold text-blue-900 mb-20 mt-5 font-Rubik" style={{ fontFamily: 'Rubik, sans-serif' }}>Онцлох үйлчилгээ</h2>
    <div className="flex flex-wrap justify-center">
    <ServiceCard title={data[0].title} description={data[0].description}/>
    <ServiceCard title={data[1].title} description={data[0].description}/>
    <div style={{ borderRadius: '40px', marginRight: '-64px'}}>
    <ServiceCard title={data[2].title} description={data[0].description}/>
    </div>
    </div>

  </div>
</section>


<section className="py-12 bg-white" style={{ fontFamily: 'Rubik, sans-serif' }}>
  <div className="container mx-auto">
    <div className="max-w-90 mx-auto bg-gray-100 rounded-3xl flex items-center">
      <div className="w-90">
        <Image src="/enhbat.jpg" alt="Image of Г. Энхбат" width={900} height={600} className="rounded-l-3xl" />
      </div>
      <div className="w-2/3 p-6">
        <blockquote className="text-black text-2xl mx-8">
          Монголын хөрөнгийн биржийн арилжааны системтэй шууд холбогдсон электрон арилжааны системийг ашиглан үнэт цаасны арилжаанд хамгийн түргэн шуурхай, цаг хугацааны хоцрогдолгүйгээр оролцох, үнэт цаасны дансаа удирдах боломжтой.
          <footer className="mt-9 font-bold text-4xl text-black">Г. Энхбат</footer>
        </blockquote>
      </div>
    </div>
  </div>
</section>


<section className="py-12 bg-gray-100">
  <div className="container mx-auto">
    <div className="flex justify-start items-center mb-10 text-black">
      <h2 className="text-6xl font-bold mr-6" style={{ fontFamily: 'Rubik, sans-serif' }}>Мэдээ мэдээлэл</h2>
      <div className="border-t border-black flex-grow relative">
        <p
          className={`absolute -top-8 left-50 right-0 text-center text-xl cursor-pointer ${
            isActiveMostRead ? 'text-green-500' : 'text-black'
          }`}
          onClick={toggleMostRead}
        >
          Most Read
        </p>
        {isActiveMostRead && (
          <div className="absolute -bottom-0 left-50 right-0 w-24 h-1 bg-green-500"></div>
        )}
        <p
          className={`text-xl absolute -top-8 right-28 cursor-pointer ${
            isActiveLatest ? 'text-green-500' : 'text-black'
          }`}
          onClick={toggleLatest}
        >
          Latest
        </p>
        {isActiveLatest && (
          <div className="absolute -bottom-0 right-28 w-14 h-1 bg-green-500"></div>
        )}
      </div>
    </div>

    <h2 className="text-2xl mb-6 text-black">On May 2, 2024</h2>

    <div className="overflow-x-scroll max-w-full">
      <div className="content-wrapper flex w-max">
        <Apple title={data[0].title} description={data[0].description} />
        <div className="flex flex-col justify-center items-center">
          <NewYork title={data[3].title} description={data[3].description} />
          <NewYork title={data[3].title} description={data[3].description} />
        </div>
        <Fed title={data[4].title} description={data[3].description} />
        <Fed title={data[4].title} description={data[3].description} />
        <Fed title={data[4].title} description={data[3].description} />
        {
          
        }
      </div>
    </div>
  </div>
</section>





        <section className="py-12 bg-gray-100">
  <div className="container mx-auto text-center">
    <h2 className="text-3xl font-bold mb-6 text-black">Онцлох мэдээ</h2>
    <div className="grid grid-cols-3 gap-6 justify-center items-center" style={{ minHeight: '200px' }}>
      <div className="bg-white p-6 rounded shadow-md rounded-3xl" style={{ minHeight: '100px' }}></div>
      <div className="bg-white p-6 rounded shadow-md rounded-3xl" style={{ minHeight: '100px' }}></div>
      <div className="bg-white p-6 rounded shadow-md rounded-3xl" style={{ minHeight: '100px' }}></div>
    </div>
  </div>
</section>

      </main>
      <footer className="bg-black text-white py-4">
        <div className="container mx-auto text-center">
          <p>&copy; 2024 TDB Securities. All rights reserved.</p>
        </div>
      </footer>
    </div>

    </div>
  );
}