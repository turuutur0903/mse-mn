'use client'
import Image from 'next/image';
import ServiceCard from '../component/common/card/service';
import data from './data.json'; // Adjust the path as needed
import Apple from '../component/common/card/service/Apple';
import { useState } from 'react';

export default function Home() {

  return (
<div style={{ fontFamily: 'Rubik, sans-serif' }}>
    <div>
      <section className="relative h-screen bg-cover bg-center" style={{ backgroundImage: 'url(/excac.jpg)' }}>
        <div className="absolute inset-0 bg-black opacity-50"></div>
        <header className="absolute top-0 left-0 w-full bg-transparent text-white py-2 z-20">
  <div className="container mx-auto flex justify-between items-center " style={{ fontFamily: 'Rubik, sans-serif' }}>
    <nav className="flex space-x-4 ml-auto">
      <a href="#" className="hover:text-gray-300 font-bold my-3">БИДНИЙ ТУХАЙ</a>
      <a href="#" className="hover:text-gray-300 font-bold my-3">ҮЙЛЧИЛГЭЭ</a>
      <a href="#" className="hover:text-gray-300 font-bold my-3">ХОЛБОО БАРИХ</a>
      <a href="#" className="hover:text-gray-300 font-bold my-3">ЗАХ ЗЭЭЛ</a>
    </nav>
  </div>
</header>

        <div className="absolute top-20 left-0 w-full bg-transparent text-white py-2 z-20">
          <div className="container mx-auto flex justify-between items-center">
            <Image src="/logo.png" alt="TDB Securities Logo" width={150} height={75} />
            <nav className="flex space-x-4">
              <a href="#" className="hover:text-gray-300 font-bold text-xl" >Хөрөнгө оруулалт</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Судалгаа</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Мэдээ</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Зах зээл</a>
              <a href="#" className="hover:text-gray-300 font-bold text-xl">Боловсрол</a>
            </nav>
            <div className="space-x-4">
              <a href="# "className="hover:text-gray-300">Нэвтрэх</a>
              <button className="bg-green-500 hover:bg-green-600 text-black px-4 py-2 rounded-full font-bold">Данс Нээх</button>
            </div>
          </div>
        </div>
        <div className="relative z-10 flex justify-center items-center h-full font-rubik">
          <div className="text-center text-white ">
            <h2 className="text-6xl font-bold font-rubik">Your Partner In Mongolian Capital Market</h2>
            <p className="mt-4 py-9 text-3xl">Your Way To Wall Street</p>
            <button className="mt-6 bg-white bg-opacity-75 hover:bg-opacity-50 text-green-700 hover:text-green-600 px-7 py-4 rounded-full font-bold text-xl">Find out more</button>
          </div>
        </div>
      </section>
      <main>
      <section className="py-12 bg-white">
  <div className="container mx-auto text-center">

    <h2 className="text-6xl font-bold text-blue-900 mb-20 mt-5 font-Rubik" style={{ fontFamily: 'Rubik, sans-serif' }}>Онцлох үйлчилгээ</h2>
    <div className="flex flex-wrap justify-center">
    <ServiceCard title={data.title} description={data.description}/>
    <ServiceCard title={data.title1} description={data.description1}/>
    <div style={{ borderRadius: '40px', marginRight: '-64px'}}>
    <ServiceCard title={data.title2} description={data.description2}/>
    </div>
    </div>

  </div>
</section>


<section className="py-12 bg-white" style={{ fontFamily: 'Rubik, sans-serif' }}>
  <div className="container mx-auto">
    <div className="max-w-90 mx-auto bg-gray-100 rounded-3xl flex items-center">
      <div className="w-90">
        <Image src="/enhbat.jpg" alt="Image of Г. Энхбат" width={900} height={600} className="rounded-l-3xl" />
      </div>
      <div className="w-2/3 p-6">
        <blockquote className="text-black text-2xl mx-8">
          Монголын хөрөнгийн биржийн арилжааны системтэй шууд холбогдсон электрон арилжааны системийг ашиглан үнэт цаасны арилжаанд хамгийн түргэн шуурхай, цаг хугацааны хоцрогдолгүйгээр оролцох, үнэт цаасны дансаа удирдах боломжтой.
          <footer className="mt-9 font-bold text-4xl text-black">Г. Энхбат</footer>
        </blockquote>
      </div>
    </div>
  </div>
</section>


        <section className="py-12 bg-gray-100">
          <div className="container mx-auto"> 
            <h2 className="text-6xl font-bold text-center mb-10 text-black">Мэдээ мэдээлэл</h2>
            <h2 className="text-2xl text-center mb-6 text-black">On May 2, 2024</h2>
            <div className="flex flex-wrap justify-center">
              
              <div className="bg-white shadow-md max-w-md mx-4 mb-6 flex flex-col" style={{ borderRadius: '20px', marginRight: '64px' }}>
                <div className="h-48 bg-cover bg-center" style={{ backgroundImage: "url('/apple.jpg')", borderTopLeftRadius: '20px', borderTopRightRadius: '20px' }}></div>
                <div className="p-6">
                  <h3 className="text-xl font-semibold mb-2 text-black text-center">Apple Inc Q2 Earnings:</h3>
                  <p className="text-gray-600 text-center">
                  On May 2, 2024, Apple Inc (NASDAQ:AAPL) disclosed its financial outcomes for the fiscal second quarter ending March 30, 2024, through an 8-K filing...
                  </p>
                </div>
              </div>
              <div className="bg-white shadow-md max-w-md mx-4 mb-6 flex flex-col" style={{ borderRadius: '20px', marginRight: '64px' }}>
                <div className="h-48 bg-cover bg-center" style={{ backgroundImage: "url('/newyork.jpg')", borderTopLeftRadius: '20px', borderTopRightRadius: '20px' }}></div>
                <div className="p-6">
                  <h3 className="text-xl font-semibold mb-2 text-black text-center">New York Stock Exchange:</h3>
                  <p className="text-gray-600 text-center">
                  The New York Stock Exchange is polling market participants on the merits of trading stocks around the clock as regulators scrutinise an application for the first 24/7 bourse...
                  </p>
                </div>
              </div>
              <div className="bg-white shadow-md max-w-md mx-4 mb-6 flex flex-col" style={{ borderRadius: '20px' }}>
                <div className="h-48 bg-cover bg-center" style={{ backgroundImage: "url('/fed.jpg')", borderTopLeftRadius: '20px', borderTopRightRadius: '20px' }}></div>
                <div className="p-6">
                  <h3 className="text-xl font-semibold mb-2 text-black text-center">Fed's Powell says:</h3>
                  <p className="text-gray-600 text-center">
                  Монголын хөрөнгийн биржийн арилжааны системтэй шууд холбогдсон электрон арилжааны системийг ашиглан 
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="py-12 bg-gray-100">
  <div className="container mx-auto text-center">
    <h2 className="text-3xl font-bold mb-6 text-black">Онцлох мэдээ</h2>
    <div className="grid grid-cols-3 gap-6 justify-center items-center" style={{ minHeight: '200px' }}>
      <div className="bg-white p-6 rounded shadow-md rounded-3xl" style={{ minHeight: '100px' }}></div>
      <div className="bg-white p-6 rounded shadow-md rounded-3xl" style={{ minHeight: '100px' }}></div>
      <div className="bg-white p-6 rounded shadow-md rounded-3xl" style={{ minHeight: '100px' }}></div>
    </div>
  </div>
</section>

      </main>
      <footer className="bg-black text-white py-4">
        <div className="container mx-auto text-center">
          <p>&copy; 2024 TDB Securities. All rights reserved.</p>
        </div>
      </footer>
    </div>

    </div>
  );
}
