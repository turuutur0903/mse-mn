import Image from 'next/image';
import { Interface } from 'readline';

interface props {
  title: string,
  description: string,
}

export default function ServiceCard(props: props) {
  const {title, description} = props;

  return (
  
      <div className="bg-gray-100 p-8 rounded-3xl shadow-md max-w-md mx-4 mb-6 flex flex-col items-center" style={{ borderRadius: '40px', marginRight: '64px',fontFamily: 'Rubik, sans-serif' }}>
        <div className="bg-blue-900 rounded-3xl h-40 w-40 mx-auto mb-4"></div>
        <h3 className="text-xl font-semibold mb-4 text-blue-900">{title}</h3>
        <p className="text-black text-center">{description}</p>
      
  </div>
  );
}

