import Image from 'next/image';
import { Interface } from 'readline';
import React, { useState } from 'react';

interface props {
  title: string,
  description: string,
  image: string,
}

export default function Apple(props: props) {
  const {title, description, image} = props;

  return (
  
    <div className="bg-gray max-w-md  mx-4 mb-6 flex flex-col" style={{ borderRadius: '20px', marginRight: '64px' }}>
    <div className="h-48 bg-cover bg-center" style={{ backgroundImage: "url('/apple.jpg')", borderTopLeftRadius: '20px', borderTopRightRadius: '20px' }}></div>
    <div className="p-6">
        <h3 className="text-3xl -ml-6 font-semibold mb-2 text-black ">Apple Inc Q2 Earnings:</h3>
        <p className="text-gray-600 -ml-6 mt-9">
            {description}
        </p>
        <p className="text-gray-600 -ml-6  mt-9">
            {description}
        </p>
        
    </div>
</div>
  );
}

