import Image from 'next/image';
import { Interface } from 'readline';
import React, { useState } from 'react';

interface props {
  title: string,
  description: string,
}

export default function NewYork(props: props) {
  const {title, description} = props;

  return (
  
    <div className="max-w-md mx-4 mb-3 flex flex-col" style={{ borderRadius: '20px', marginRight: '64px' }}>
            <div className="h-48 bg-cover bg-center" style={{ backgroundImage: "url('/newyork.jpg')", borderTopLeftRadius: '20px', borderTopRightRadius: '20px' }}>
                </div>
             <div className="p-6">
                <h3 className="text-3xl font-semibold mb-2 text-black -ml-6">{title}</h3>
                <p className="text-gray-600 -ml-6">
                {description}
                    </p>
             </div>
        </div>
    
       
  );
}

