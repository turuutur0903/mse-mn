import Image from 'next/image';
import { Interface } from 'readline';
import React, { useState } from 'react';

interface props {
  title: string,
  description: string,
}

export default function Fed(props: props) {
  const {title, description} = props;

  return (
  
    <div className="max-w-md mx-4 mb-6 flex flex-col" style={{ borderRadius: '20px' }}>
        <div className="h-48 bg-cover bg-center" style={{ backgroundImage: "url('/fed.jpg')", borderTopLeftRadius: '20px', borderTopRightRadius: '20px' }}></div>
        <div className="p-6">
            <h3 className="text-3xl font-semibold mb-2 text-black -ml-6">{title}</h3>
            <p className="text-gray-600  -ml-6 mt-9">
               {description}
            </p>
            <p className="text-gray-600 -ml-6  mt-9">
            {description}
        </p>
        </div>
    </div>
  );
}

