// TextClickHandler.tsx
import React from 'react';

interface TextClickHandlerProps {
  text: string;
  onClick: (text: string) => void;
}

const TextClickHandler: React.FC<TextClickHandlerProps> = ({ text, onClick }) => {
  return (
    <p
      className="text-xl cursor-pointer clickable-text"
      style={{ color: 'black' }}
      onClick={() => onClick(text)}
    >
      {text}
    </p>
  );
};

export default TextClickHandler;
